package com.tp_homework3.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "date_of_order")
	private String date;
	@ManyToOne()
	@JoinColumn(name = "client_id")
	private Clients client;
	@OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
	private Set<OrderedProduct> orderedProducts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Clients getClient() {
		return client;
	}

	public void setClient(Clients client) {
		this.client = client;
	}

	public Set<OrderedProduct> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(Set<OrderedProduct> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}

	public Map<String, String> getAsMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("id", String.valueOf(id));
		map.put("date_of_order", date);
		map.put("client_id", client.toString());
		return map;
	}
}
