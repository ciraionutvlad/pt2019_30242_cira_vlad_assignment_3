package com.tp_homework3.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "clients")
public class Clients {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "first_name")
	private String first_name;
	@Column(name = "last_name")
	private String last_name;
	@Column(name = "email")
	private String email;
	@OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
	private Set<Orders> orders;

	public int isId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return first_name;
	}

	public void setFirstname(String firstname) {
		this.first_name = firstname;
	}

	public String getLastname() {
		return last_name;
	}

	public void setLastname(String lastname) {
		this.last_name = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Orders> getOrders() {
		return orders;
	}

	public void setOrders(Set<Orders> orders) {
		this.orders = orders;
	}

	public Map<String, String> getAsMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		 map.put("id", String.valueOf(id));
		map.put("first_name", first_name);
		map.put("last_name", last_name);
		map.put("email", email);
		return map;
	}

	@Override
	public String toString() {
		return "Clients [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", email=" + email
				+ ", orders=" + orders + "]";
	}
	
	
}
