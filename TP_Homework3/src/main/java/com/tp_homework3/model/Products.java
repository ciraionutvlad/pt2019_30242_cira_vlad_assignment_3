package com.tp_homework3.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Products {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "product_name")
	private String productName;
	@Column(name = "price")
	private int price;
	@Column(name = "stock")
	private int stock;
	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private Set<OrderedProduct> orderedProducts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Set<OrderedProduct> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(Set<OrderedProduct> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}

	public Map<String, String> getAsMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("id", String.valueOf(id));
		map.put("product_name", productName);
		map.put("price", String.valueOf(price));
		map.put("stock", String.valueOf(stock));
		return map;
	}
}
