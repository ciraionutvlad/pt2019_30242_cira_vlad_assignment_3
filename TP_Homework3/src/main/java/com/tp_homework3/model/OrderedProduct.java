package com.tp_homework3.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ordered_products")
public class OrderedProduct {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "quantity")
	private int quantity;
	@ManyToOne()
	@JoinColumn(name = "order_id")
	private Orders order_id;
	@ManyToOne()
	@JoinColumn(name = "product_id")
	private Products product_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Orders getOrder() {
		return order_id;
	}

	public void setOrder(Orders order) {
		this.order_id = order;
	}

	public Products getProduct() {
		return product_id;
	}

	public void setProduct(Products product) {
		this.product_id = product;
	}

	public Map<String, String> getAsMap() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("id", String.valueOf(id));
		map.put("quantity", String.valueOf(quantity));
		return map;
	}
}
