package com.tp_homework3.business;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.tp_homework3.dao.OrderDAO;
import com.tp_homework3.dao.OrderedProductDAO;
import com.tp_homework3.dao.ProductDAO;
import com.tp_homework3.model.Clients;
import com.tp_homework3.model.Orders;
import com.tp_homework3.model.OrderedProduct;
import com.tp_homework3.model.Products;

public class PlaceOrder {

	private ProductDAO productDAO;
	private OrderDAO orderDAO;
	private OrderedProductDAO orderedProductDAO;

	public PlaceOrder() {
		this.productDAO = new ProductDAO();
		this.orderDAO = new OrderDAO();
		this.orderedProductDAO = new OrderedProductDAO();
	}

	public boolean isUnderStock(int productId, int quantity) {
		boolean isUnderStock = false;
		Products product = productDAO.getById(productId);

		if (product.getStock() < quantity) {
			isUnderStock = true;
		}

		return isUnderStock;
	}

	public void placeOrder(Clients client, Set<OrderedProduct> orderedProducts) {
		Orders order = new Orders();
		order.setClient(client);
		order.setDate(getCurrentDateString());

		orderDAO.create(order.getAsMap());

		Orders lastOrder = orderDAO.getAll().get(0);

		for (OrderedProduct product : orderedProducts) {
			product.setOrder(lastOrder);

			// create table if doesn't exist
			orderedProductDAO.createTable(product);
			// insert entry in table
			orderedProductDAO.create(product.getAsMap());

			// decrement stock
			Products newProduct = productDAO.getById(product.getId());
			int newStock = newProduct.getStock() - product.getQuantity();
			newProduct.setStock(newStock);

			// update decremented stock for the given product
			productDAO.update(newProduct.getAsMap());
		}

		// create bill file
		createBill(client, order, orderedProducts);
	}

	private void createBill(Clients client, Orders order, Set<OrderedProduct> orderedProducts) {
		File file = new File("bill.txt");
		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(file, false);
			
			if (!file.exists()) {
				file.createNewFile();
			}

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Hi " + client.getFirstname() + "!. Thank you for your purchase! \n");
			stringBuilder.append("Order date: " + order.getDate() + "\n");
			stringBuilder.append("Purchased products: \n");

			for (OrderedProduct product : orderedProducts) {
				stringBuilder.append("-> " + "Product name: " + product.getProduct().getProductName() + ", price: "
						+ product.getProduct().getPrice() + ", quantity: " + product.getQuantity() + "\n");
			}

			fileOutputStream.write(stringBuilder.toString().getBytes());
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	private String getCurrentDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

}
