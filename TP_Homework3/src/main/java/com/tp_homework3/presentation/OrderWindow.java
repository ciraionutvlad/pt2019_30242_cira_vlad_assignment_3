package com.tp_homework3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

public class OrderWindow extends JFrame {

	private JPanel panel;
	private JLabel clientIDLabel;
	private JLabel productIDLabel;
	private JLabel quantityLabel;

	private JTextField clientIDTextField;
	private JTextField productIDTextField;
	private JTextField quantityTextField;

	private JButton addOrderButton;
	private JButton deleteOrderButton;
	private JButton viewAllOrdersButton;

	private JButton backButton;
	private JTable orderJTable;

	public OrderWindow() {
		// Setting the width and the height of the frame
		setSize(850, 490);

		// Closing the frame by clicking the X button
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Initialise the ClientWindow components
		initialiseOrderWindowComponents();

		// Displaying the frame
		setVisible(true);
	}

	public static void main(String[] args) {
		// System.out.println( "Hello World!" );
		OrderWindow orderWindow = new OrderWindow();
	}

	private void initialiseOrderWindowComponents() {
		// Create the panel
		panel = new JPanel();

		// Setting the layout null
		panel.setLayout(null);

		// Label for Client ID
		clientIDLabel = new JLabel("Client ID :");
		clientIDLabel.setBounds(10, 20, 80, 25);
		panel.add(clientIDLabel);
		// TextField for Client IDe
		clientIDTextField = new JTextField();
		clientIDTextField.setBounds(90, 20, 220, 25);
		panel.add(clientIDTextField);

		// Label for First Name
		productIDLabel = new JLabel("Product ID :");
		productIDLabel.setBounds(10, 50, 80, 25);
		panel.add(productIDLabel);
		// TextField for First Name
		productIDTextField = new JTextField();
		productIDTextField.setBounds(90, 50, 220, 25);
		panel.add(productIDTextField);

		// Label for First Last Name
		quantityLabel = new JLabel("Quantity :");
		quantityLabel.setBounds(10, 80, 80, 25);
		panel.add(quantityLabel);
		// TextField for Last Name
		quantityTextField = new JTextField();
		quantityTextField.setBounds(90, 80, 220, 25);
		panel.add(quantityTextField);

		// Button for Add new client
		addOrderButton = new JButton("Adaugare comanda");
		addOrderButton.setBounds(10, 200, 160, 25);
		panel.add(addOrderButton);

		// Button for Delete client
		deleteOrderButton = new JButton("Realizare comanda");
		deleteOrderButton.setBounds(10, 230, 160, 25);
		panel.add(deleteOrderButton);

		// Button for View all clients
		viewAllOrdersButton = new JButton("Vizualizare comenzi");
		viewAllOrdersButton.setBounds(10, 260, 160, 25);
		panel.add(viewAllOrdersButton);

		// Button for Back
		backButton = new JButton("Inapoi");
		backButton.setBounds(10, 400, 160, 25);
		panel.add(backButton);

		// JTable for clients
		orderJTable = new JTable();
		orderJTable.setBounds(330, 20, 480, 410);
		panel.add(orderJTable);

		// If the button for Add new client is pressed
		addOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Edit client is pressed
		deleteOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Delete client is pressed
		viewAllOrdersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Back new client is pressed
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// Adding the panel to frame
		add(panel);
	}

}
