package com.tp_homework3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.tp_homework3.dao.ClientDAO;
import com.tp_homework3.dao.ProductDAO;
import com.tp_homework3.model.Clients;
import com.tp_homework3.model.Products;

public class ProductWindow extends JFrame {

	private JPanel panel;
	private JLabel productIDLabel;
	private JLabel productNameLabel;
	private JLabel priceLabel;
	private JLabel stockLabel;
	private JTextField productIDTextField;
	private JTextField productNameTextField;
	private JTextField priceTextField;
	private JTextField stockTextField;
	private JButton addProductButton;
	private JButton editProductButton;
	private JButton deleteProductButton;
	private JButton viewAllProductsButton;
	// private JButton orderButton;
	private JButton backButton;
	private JTable productJTable;

	public ProductWindow() {
		// Setting the width and the height of the frame
		setSize(850, 490);

		// Closing the frame by clicking the X button
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Initialise the ClientWindow components
		initialiseProductWindowComponents();

		// Displaying the frame
		setVisible(true);
	}

	public static void main(String[] args) {
		// System.out.println( "Hello World!" );
		ProductWindow productWindow = new ProductWindow();
	}

	private void initialiseProductWindowComponents() {
		// Create the panel
		panel = new JPanel();

		// Setting the layout null
		panel.setLayout(null);

		// Label for Client ID
		productIDLabel = new JLabel("Product ID :");
		productIDLabel.setBounds(10, 20, 100, 25);
		panel.add(productIDLabel);
		// TextField for Client IDe
		productIDTextField = new JTextField();
		productIDTextField.setBounds(110, 20, 200, 25);
		panel.add(productIDTextField);

		// Label for First Name
		productNameLabel = new JLabel("Product Name :");
		productNameLabel.setBounds(10, 50, 100, 25);
		panel.add(productNameLabel);
		// TextField for First Name
		productNameTextField = new JTextField();
		productNameTextField.setBounds(110, 50, 200, 25);
		panel.add(productNameTextField);

		// Label for First Last Name
		priceLabel = new JLabel("Price :");
		priceLabel.setBounds(10, 80, 100, 25);
		panel.add(priceLabel);
		// TextField for Last Name
		priceTextField = new JTextField();
		priceTextField.setBounds(110, 80, 200, 25);
		panel.add(priceTextField);

		// Label for Email
		stockLabel = new JLabel("Stock :");
		stockLabel.setBounds(10, 110, 100, 25);
		panel.add(stockLabel);
		// TextField for Email
		stockTextField = new JTextField();
		stockTextField.setBounds(110, 110, 200, 25);
		panel.add(stockTextField);

		// Button for Add new client
		addProductButton = new JButton("Adaugare produs nou");
		addProductButton.setBounds(10, 200, 160, 25);
		panel.add(addProductButton);

		// Button for Edit client
		editProductButton = new JButton("Editare produs");
		editProductButton.setBounds(10, 230, 160, 25);
		panel.add(editProductButton);

		// Button for Delete client
		deleteProductButton = new JButton("Stergere produs");
		deleteProductButton.setBounds(10, 260, 160, 25);
		panel.add(deleteProductButton);

		// Button for View all clients
		viewAllProductsButton = new JButton("Vizualizare produse");
		viewAllProductsButton.setBounds(10, 290, 160, 25);
		panel.add(viewAllProductsButton);

		/*
		 * orderButton = new JButton("Comenzi"); orderButton.setBounds(10, 320, 160,
		 * 25); panel.add(orderButton);
		 */

		// Button for Back
		backButton = new JButton("Inapoi");
		backButton.setBounds(10, 400, 160, 25);
		panel.add(backButton);

		// JTable for clients
		productJTable = new JTable();
		productJTable.setBounds(330, 20, 480, 410);
		panel.add(productJTable);

		// If the button for Add new client is pressed
		addProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// String id = clientIDTextField.getText();
				String product_name = productNameTextField.getText();
				int price = Integer.parseInt(priceTextField.getText());
				int stock = Integer.parseInt(stockTextField.getText());
				Products product = new Products();
				// client.setId(Integer.parseInt(id));
				product.setProductName(product_name);
				product.setPrice(price);
				product.setStock(stock);
				ProductDAO productDAO = new ProductDAO();
				productDAO.create(product.getAsMap()); // string.valueof
			}
		});

		// If the button for Edit client is pressed
		editProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Delete client is pressed
		deleteProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for View all clients is pressed
		viewAllProductsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Back new client is pressed
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// Adding the panel to frame
		add(panel);
	}
}
