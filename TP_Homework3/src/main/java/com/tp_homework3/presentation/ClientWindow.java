package com.tp_homework3.presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.tp_homework3.dao.ClientDAO;
import com.tp_homework3.model.Clients;

public class ClientWindow extends JFrame {

	private JPanel panel;
	private JLabel clientIDLabel;
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel emailLabel;
	private JTextField clientIDTextField;
	private JTextField firstNameTextField;
	private JTextField lastNameTextField;
	private JTextField emailTextField;
	private JButton addClientButton;
	private JButton editClientButton;
	private JButton deleteClientButton;
	private JButton viewAllClientsButton;
	private JButton productButton;
	private JButton backButton;
	private JTable clientJTable;

	public ClientWindow() {
		// Setting the width and the height of the frame
		setSize(850, 490);

		// Closing the frame by clicking the X button
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Initialise the ClientWindow components
		initialiseClientWindowComponents();

		// Displaying the frame
		setVisible(true);
	}

	public static void main(String[] args) {
		// System.out.println( "Hello World!" );
		ClientWindow clientWindow = new ClientWindow();
	}

	private void initialiseClientWindowComponents() {
		// Create the panel
		panel = new JPanel();

		// Setting the layout null
		panel.setLayout(null);

		// Label for Client ID
		clientIDLabel = new JLabel("Client ID :");
		clientIDLabel.setBounds(10, 20, 80, 25);
		panel.add(clientIDLabel);
		// TextField for Client IDe
		clientIDTextField = new JTextField();
		clientIDTextField.setBounds(90, 20, 220, 25);
		panel.add(clientIDTextField);

		// Label for First Name
		firstNameLabel = new JLabel("First Name :");
		firstNameLabel.setBounds(10, 50, 80, 25);
		panel.add(firstNameLabel);
		// TextField for First Name
		firstNameTextField = new JTextField();
		firstNameTextField.setBounds(90, 50, 220, 25);
		panel.add(firstNameTextField);

		// Label for First Last Name
		lastNameLabel = new JLabel("Last Name :");
		lastNameLabel.setBounds(10, 80, 80, 25);
		panel.add(lastNameLabel);
		// TextField for Last Name
		lastNameTextField = new JTextField();
		lastNameTextField.setBounds(90, 80, 220, 25);
		panel.add(lastNameTextField);

		// Label for Email
		emailLabel = new JLabel("Email :");
		emailLabel.setBounds(10, 110, 80, 25);
		panel.add(emailLabel);
		// TextField for Email
		emailTextField = new JTextField();
		emailTextField.setBounds(90, 110, 220, 25);
		panel.add(emailTextField);

		// Button for Add new client
		addClientButton = new JButton("Adaugare client nou");
		addClientButton.setBounds(10, 200, 160, 25);
		panel.add(addClientButton);

		// Button for Edit client
		editClientButton = new JButton("Editare client");
		editClientButton.setBounds(10, 230, 160, 25);
		panel.add(editClientButton);

		// Button for Delete client
		deleteClientButton = new JButton("Stergere client");
		deleteClientButton.setBounds(10, 260, 160, 25);
		panel.add(deleteClientButton);

		// Button for View all clients
		viewAllClientsButton = new JButton("Vizualizare clienti");
		viewAllClientsButton.setBounds(10, 290, 160, 25);
		panel.add(viewAllClientsButton);

		// Button for Product
		productButton = new JButton("Produse");
		productButton.setBounds(10, 320, 160, 25);
		panel.add(productButton);

		// Button for Back
		backButton = new JButton("Inapoi");
		backButton.setBounds(10, 400, 160, 25);
		panel.add(backButton);

		// JTable for clients
		clientJTable = new JTable();
		clientJTable.setBounds(330, 20, 480, 410);
		panel.add(clientJTable);

		// If the button for Add new client is pressed
		addClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// String id = clientIDTextField.getText();
				String first_name = firstNameTextField.getText();
				String last_name = lastNameTextField.getText();
				String email = emailTextField.getText();
				Clients client = new Clients();
				// client.setId(Integer.parseInt(id));
				client.setFirstname(first_name);
				client.setLastname(last_name);
				client.setEmail(email);
				ClientDAO clientDAO = new ClientDAO();
				clientDAO.create(client.getAsMap());
			}
		});

		// If the button for Edit client is pressed
		editClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(clientIDTextField.getText());
				String first_name = firstNameTextField.getText();
				String last_name = lastNameTextField.getText();
				String email = emailTextField.getText();
				Clients client = new Clients();
				client.setId(id);
				client.setFirstname(first_name);
				client.setLastname(last_name);
				client.setEmail(email);
				ClientDAO clientDAO = new ClientDAO();
				clientDAO.update(client.getAsMap());
			}
		});

		// If the button for Delete client is pressed
		deleteClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(clientIDTextField.getText());
				ClientDAO clientDAO = new ClientDAO();
				clientDAO.delete(id);
			}
		});

		// If the button for View all clients is pressed
		viewAllClientsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Product is pressed
		productButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// If the button for Back new client is pressed
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
			}
		});

		// Adding the panel to frame
		add(panel);
	}
}
