package com.tp_homework3.dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;

import com.tp_homework3.database.ConnectionFactory;
import com.tp_homework3.model.OrderedProduct;

public class OrderedProductDAO extends AbstractDAO<OrderedProduct> {

	public void createTable(OrderedProduct product) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("CREATE TABLE IF NOT EXISTS tp_homework3.`ordered_products` (\n");
		String id = "";

		for (Field field : OrderedProduct.class.getDeclaredFields()) {
			// set modifier to public first
			field.setAccessible(true);
			String name = field.getName();
			String type = field.getType().getName();

			String dbType;
			if (name.equals("id")) {
				dbType = "INT UNSIGNED NOT NULL AUTO_INCREMENT";
				id = name;
			} else if (type.equals("String")) {
				dbType = "VARCHAR(50) NOT NULL";
			} else {
				dbType = "INT UNSIGNED NOT NULL";
			}

			stringBuilder.append("'" + name + "'" + dbType);
		}

		stringBuilder.append("PRIMARY KEY ('" + id + "')\n");
		stringBuilder.append(");");

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(stringBuilder.toString());
			statement.executeQuery();

			connectionFactory.closeConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
