package com.tp_homework3.dao;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tp_homework3.database.ConnectionFactory;

public class AbstractDAO<T> {

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public void create(Map<String, String> map) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery(map);
		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(query);
			statement.executeUpdate();

			connectionFactory.closeConnection();
			connectionFactory.closeStatement(statement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void update(Map<String, String> map) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createUpdateQuery(map, "id");
		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(query);
			statement.setInt(1, Integer.valueOf(map.get("id")));
			statement.executeUpdate();

			connectionFactory.closeConnection();
			connectionFactory.closeStatement(statement);
		} catch (Exception ex) {

		}
	}

	public T getById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		T result = null;
		String query = createGetByIdQuery("id");
		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			List<T> list = createObjects(resultSet);

			if (list != null && !list.isEmpty()) {
				result = createObjects(resultSet).get(0);
			}

			// connectionFactory.closeConnection();
			// connectionFactory.closeStatement(statement);
			// connectionFactory.closeResultSet(resultSet);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public List<T> getAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<T> result = new ArrayList<T>();
		String query = createGetAllQuery();
		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			result = createObjects(resultSet);

			connectionFactory.closeConnection();
			connectionFactory.closeStatement(statement);
			connectionFactory.closeResultSet(resultSet);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public void delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteQuery("id");
		try {
			ConnectionFactory connectionFactory = ConnectionFactory.newInstance();
			connection = connectionFactory.getConnection();

			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();

			connectionFactory.closeConnection();
			connectionFactory.closeStatement(statement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();
		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					// if (field.getType().isPrimitive()) {
					Object value = resultSet.getObject(field.getName());
					System.out.print("6 - " + value.toString() + "\n");
					System.out.print("6 - " + field.getName() + "\n");
					System.out.print("1 - " + field.getType() + "\n");

					PropertyDescriptor descriptior = new PropertyDescriptor(field.getName(), field.getType());
					Method method = descriptior.getWriteMethod();
					System.out.print("2 - " + method.toString() + "\n");
					// System.out.print("2." + value.toString());
					System.out.print("5 - " + instance.toString() + "\n");

					method.setAccessible(true);
					method.invoke(instance, value);
					// }

				}
				System.out.print("3 - " + instance.toString());
				list.add(instance);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return list;
	}

	private String createInsertQuery(Map<String, String> map) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("INSERT INTO ");
		stringBuilder.append(type.getSimpleName() + " ");
		stringBuilder.append(" VALUES(");

		for (Map.Entry<String, String> entry : map.entrySet()) {
			stringBuilder.append("'");
			stringBuilder.append(entry.getValue());
			stringBuilder.append("'");
			stringBuilder.append(",");
		}

		stringBuilder = new StringBuilder(stringBuilder.subSequence(0, stringBuilder.length() - 1));
		stringBuilder.append(")");

		System.out.print(stringBuilder.toString());

		return stringBuilder.toString();
	}

	private String createUpdateQuery(Map<String, String> map, String field) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("UPDATE ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append("SET ");

		for (Map.Entry<String, String> entry : map.entrySet()) {
			stringBuilder.append(entry.getKey());
			stringBuilder.append(" = ");
			stringBuilder.append("'");
			stringBuilder.append(entry.getValue());
			stringBuilder.append("'");
			stringBuilder.append(",");
		}

		stringBuilder = new StringBuilder(stringBuilder.subSequence(0, stringBuilder.length() - 1));
		stringBuilder.append("WHERE " + field + " =?");
		stringBuilder.append(";");
		return stringBuilder.toString();
	}

	private String createGetByIdQuery(String field) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT * FROM ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" WHERE " + field + " =?");
		System.out.print(stringBuilder.toString());
		return stringBuilder.toString();
	}

	private String createGetAllQuery() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT * FROM ");
		stringBuilder.append(type.getSimpleName());
		return stringBuilder.toString();
	}

	private String createDeleteQuery(String field) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("DELETE FROM ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" WHERE " + field + " =?");
		return stringBuilder.toString();
	}

}
